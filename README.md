* For installing clipboard on vim:
  https://vi.stackexchange.com/questions/84/how-can-i-copy-text-to-the-system-clipboard-from-vim
  On Ubuntu:
    sudo apt install vim-gtk3
  
* For getting rid of the annoying space after apostrophe change keyboard to English(US) instead of English(INT)

* Install The Silver Searcher:
  https://github.com/ggreer/the_silver_searcher

* Install scm_breeze:
  https://github.com/scmbreeze/scm_breeze

* To toggle hidden files on vim:
  Shift + I

* Nice tutorial to install virtualenv and virtualenvwrapper:
  https://www.freecodecamp.org/news/virtualenv-with-virtualenvwrapper-on-ubuntu-18-04/

* Install zsh:
  https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH
* Install oh-my-zsh:
  https://www.tecmint.com/install-oh-my-zsh-in-ubuntu/

TODO:

* Add .oh-my-zsh to repo
* Add .spacemacs config to repo


